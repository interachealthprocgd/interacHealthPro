import { useState } from "react"

export function useForm(data){
    const [form, setState] = useState(data)

    const setForm = (field) => 
        (e) => setState(state => ({
            ...state, 
            [field] : e.target.value
        }))

    return {form, setForm}
}