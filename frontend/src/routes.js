import { createBrowserRouter } from "react-router-dom";
import { App } from "./App";
import { GetEmployee } from "./pages/employee/GetEmployee";
import { PostEmployee } from "./pages/employee/PostEmployee";
import { RegisterBusiness } from "./pages/auth/RegisterBusiness";
import { RegisterClient } from "./pages/auth/RegisterClient";
import { UpdateEmployee } from "./pages/employee/UpdateEmployee";
import { DeleteEmployee } from "./pages/employee/DeleteEmployee";
import { GetEmployeeGroup } from "./pages/employeeGroup/GetEmployeeGroup";
import { PostGroupEmployee } from "./pages/employeeGroup/PostGroupEmployee";
import { PutEmployeeGroup } from "./pages/employeeGroup/PutEmployeeGroup";
import { GetAllPaymentInfo } from "./pages/paymentInfo/GetPaymentInfo";
import { PostPaymentInfo } from "./pages/paymentInfo/PostPaymentInfo";
import { PutPaymentInfo } from "./pages/paymentInfo/PutPaymentInfo";
import { DeletePaymentInfo } from "./pages/paymentInfo/DeletePaymentInfo";
import { PostInvoice } from "./pages/invoice/PostInvoice";
import { GetInvoiceClient } from "./pages/clientInvoice/GetInvoiceClient";
import { PayInvoice } from "./pages/invoice/PayInvoice";
import { Login } from "./pages/auth/Login";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
     
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/register/client",
        element: <RegisterClient />,
      },
      {
        path: "/register/business",
        element: <RegisterBusiness />,
      },
      {
        path: "/employee/get",
        element: <GetEmployee />,
      },
      {
        path: "/employee/post",
        element: <PostEmployee />,
      },
      {
        path: "/employee/put",
        element: <UpdateEmployee />,
      },
      {
        path: "/employee/delete",
        element: <DeleteEmployee />,
      },
      {
        path: "/groupEmployee/get",
        element: <GetEmployeeGroup />,
      },
      {
        path: "/groupEmployee/post",
        element: <PostGroupEmployee />,
      },
      {
        path: "/groupEmployee/put",
        element: <PutEmployeeGroup />,
      },
      {
        path: "/paymentInfo/get",
        element: <GetAllPaymentInfo />,
      },
      {
        path: "/paymentInfo/post",
        element: <PostPaymentInfo />,
      },
      {
        path: "/paymentInfo/put",
        element: <PutPaymentInfo />,
      },
      {
        path: "/paymentInfo/delete",
        element: <DeletePaymentInfo />,
      },
      {
        path : "/invoice/post",
        element : <PostInvoice />
      },
      {
        path : "/client/invoice/get",
        element : <GetInvoiceClient />
      },
      {
        path : "/payInvoice",
        element : <PayInvoice />
      }
    ],
  },
]);
