import { configureStore } from "@reduxjs/toolkit";
import { authReducer } from "./slices/authSlice";
import { infoReducer } from "./slices/infoSlice";

export const store = configureStore({
    reducer : {
        infoSlice : infoReducer,
        authSlice : authReducer
    }
})