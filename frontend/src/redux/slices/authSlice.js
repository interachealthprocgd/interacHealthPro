import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
    name : "authSlice",

    initialState : {
        idUser : null,
        email : null,
        accountType : null
    },

    reducers : {
        updateStatus(state, action){
            state.idUser = action.payload.idUser
            state.email = action.payload.email
            state.accountType = action.payload.role
        },

        reset(state, action){
            state.idUser = null
            state.email = null
            state.accountType = null
        }
    }
})

export const authSlice = slice.actions
export const authReducer = slice.reducer