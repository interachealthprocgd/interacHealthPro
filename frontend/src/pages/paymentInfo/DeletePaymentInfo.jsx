import { Button, Form } from "react-bootstrap";
import { useApi } from "../../hooks/useApi";
import { useForm } from "../../hooks/useForm";

export function DeletePaymentInfo() {
  const { request } = useApi();
  const { form, setForm } = useForm({
    idPaymentInfo: "",
  });

  const onSubmit = (e) => {
    e.preventDefault();
    request("/paymentInfo/deletePaymentInfo", "delete", form);
  };

  return (
    <Form onSubmit={onSubmit}>
      <h3>Delete Payment Information</h3>
      <Form.Control
        className="my-3"
        type="text"
        placeholder="id Payment Information"
        value={form.idPaymentInfo}
        onChange={setForm("idPaymentInfo")}
      />
      <Button className="my-3" variant="primary" type="submit">
        Submit
      </Button>
    </Form>
  );
}
