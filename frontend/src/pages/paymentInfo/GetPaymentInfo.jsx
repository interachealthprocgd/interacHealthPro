import { useEffect, useState } from "react";
import { useApi } from "../../hooks/useApi";

export function GetAllPaymentInfo() {
  const [state, setState] = useState([]);
  const { request } = useApi();

  const showData = () =>
    state.map((pI) => (
      <tr key={pI.idPaymentInfo}>
        <td> {pI.idPaymentInfo} </td>
        <td>{pI.cardHolder}</td>
        <td>{pI.numCard}</td>
        <td>{pI.typeCard}</td>
        <td> {pI.expDate} </td>
        <td>{pI.secCode}</td>
        <td>{" " + pI.accActive}</td>
        <td>{pI.lastUpdateDate}</td>
        <td>{"" + pI.idAcc}</td>
        <td>{"" + pI.idAccEnt}</td>
      </tr>
    ));

  const getData = async () => {
    const response = await request("/paymentInfo/getAllPaymentInfo");

    if (response) setState(response);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <h2>Get all payment information </h2>
      <table className="table">
        <thead>
          <tr>
            <td>idPaymentInfo</td>
            <td>cardHolder</td>
            <td>numCard</td>
            <td>typeCard</td>
            <td>expDate</td>
            <td>secCode</td>
            <td>accActive</td>
            <td>lastUpdateDate</td>
            <td>idAcc</td>
            <td>idAccEnt</td>
          </tr>
        </thead>
        <tbody>{showData()}</tbody>
      </table>
    </>
  );
}
