import { Button, Form } from "react-bootstrap";
import { useApi } from "../../hooks/useApi";
import { useForm } from "../../hooks/useForm";

const TypeCard = {
  VISA: "Visa",
  MASTERCARD: "MasterCard",
  AMERICANEXPRESS: "American Express",
};

export function PutPaymentInfo() {
  const { request } = useApi();
  const { form, setForm } = useForm({
    idPaymentInfo: "",
    cardHolder: "",
    numCard: "",
    typeCard: "",
    expDate: "",
    secCode: "",
  });

  const onSubmit = async (f) => {
    f.preventDefault();
    const data = {
      ...form,
      cardHolder: form.cardHolder === "" ? null : form.cardHolder,
      numCard: form.numCard === "" ? null : form.numCard,
      typeCard: form.typeCard === "" ? null : form.typeCard,
      expDate: form.expDate === "" ? null : form.expDate,
      secCode: form.secCode === "" ? null : form.secCode,
    };

    request("/paymentInfo/updatePaymentInfo", "put", data);
  };

  return (
    <Form onSubmit={onSubmit}>
      <h3>Update a payment information</h3>

      <Form.Control
        className="my-3"
        type="text"
        placeholder="idPaymentInfo"
        value={form.idPaymentInfo}
        onChange={setForm("idPaymentInfo")}
      />

      <Form.Control
        className="my-3"
        type="text"
        placeholder="cardHolder"
        value={form.cardHolder}
        onChange={setForm("cardHolder")}
      />

      <Form.Control
        className="my-3"
        type="text"
        placeholder="numCard"
        value={form.numCard}
        onChange={setForm("numCard")}
      />

      <Form.Select value={form.typeCard} onChange={setForm("typeCard")}>
        <option value="">null - Payment by Paypal</option>

        <option key={TypeCard.VISA} value={TypeCard.VISA}>{`Visa`}</option>

        <option
          key={TypeCard.MASTERCARD}
          value={TypeCard.MASTERCARD}
        >{`MasterCard`}</option>

        <option
          key={TypeCard.AMERICANEXPRESS}
          value={TypeCard.AMERICANEXPRESS}
        >{`American Express`}</option>
      </Form.Select>

      <Form.Control
        className="my-3"
        type="text"
        placeholder="expDate"
        value={form.expDate}
        onChange={setForm("expDate")}
      />

      <Form.Control
        className="my-3"
        type="text"
        placeholder="secCode"
        value={form.secCode}
        onChange={setForm("secCode")}
      />

      <Button className="my-3" variant="primary" type="submit">
        Submit
      </Button>
    </Form>
  );
}
