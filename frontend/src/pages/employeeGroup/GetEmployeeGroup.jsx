import { useEffect, useState } from "react";
import { useApi } from "../../hooks/useApi";

export function GetEmployeeGroup() {
  const [state, setState] = useState([]);
  const { request } = useApi();

  const showData = () =>
    state.map((eg) => (
      <tr key={eg.idGroup}>
        <td> {eg.idGroup} </td>
        <td>{eg.groupName}</td>
        <td>{"" + eg.accActive}</td>
        <td>{eg.lastUpdateDate}</td>
        <td>{eg.idAccEnt}</td>
      </tr>
    ));

  const getData = async () => {
    const response = await request("/business/groupEmployee/getAll");

    if (response) setState(response);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <h2>Get all employee groups </h2>
      <table className="table">
        <thead>
          <tr>
            <td>idGroup</td>
            <td>groupName</td>
            <td>accActive</td>
            <td>lastUpdateDate</td>
            <td>idAccEnt</td>
          </tr>
        </thead>
        <tbody>{showData()}</tbody>
      </table>
    </>
  );
}
