import { Button, Form } from "react-bootstrap";
import { useApi } from "../../hooks/useApi";
import { useForm } from "../../hooks/useForm";

export function PostGroupEmployee() {
  const { request } = useApi();
  const { form, setForm } = useForm({
    groupName: ""
  });

  const onSubmit = async (f) => {
    f.preventDefault();
    request("/business/groupEmployee/createGroup", "post", form);
  };

  return (
    <Form onSubmit={onSubmit}>
      <h3>Create Employee group</h3>
      <Form.Control
        className="my-3"
        type="text"
        placeholder="groupName"
        value={form.groupName}
        onChange={setForm("groupName")}
      />

      <Button className="my-3" variant="primary" type="submit">
        Submit
      </Button>
    </Form>
  );
}
