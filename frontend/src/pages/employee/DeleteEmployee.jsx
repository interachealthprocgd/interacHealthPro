import { Button, Form } from "react-bootstrap"
import { useApi } from "../../hooks/useApi"
import { useForm } from "../../hooks/useForm"

export function DeleteEmployee(){
    const {request} = useApi()
    const {form, setForm} = useForm({
        idAcc : ""
    })

    const onSubmit = (e)=>{
        e.preventDefault()
        request("/business/employee", "delete", form)
    }

    return (
        <Form onSubmit={onSubmit}>
            <h3>Delete employee</h3>
            <Form.Control className="my-3" type="text" placeholder="id account" value={form.idAcc} onChange={setForm("idAcc")} />
            <Button className="my-3" variant="primary" type="submit">Submit</Button>
        </Form>
    )
}