import { Form } from "react-bootstrap";
import Button from "react-bootstrap/esm/Button";
import { useApi } from "../../hooks/useApi";
import { useForm } from "../../hooks/useForm";

export function RegisterClient(){

    const {request} = useApi()
    const {form, setForm} = useForm({
        idAcc : "",
        name : "",
        lastName : "",
        email : "", 
        password : ""
    })

    const onSubmit = async (e) => {
        e.preventDefault()
        request("/auth/register/client", "post", form)
    }

    return (
        <Form onSubmit={onSubmit}>
            <h3>Create client</h3>
            <Form.Control className="my-3" type="text" placeholder="id" value={form.idAcc} onChange={setForm("idAcc")} />
            <Form.Control className="my-3" type="text" placeholder="name" value={form.name} onChange={setForm("name")} />
            <Form.Control className="my-3" type="text" placeholder="lastName" value={form.lastName} onChange={setForm("lastName")} />
            <Form.Control className="my-3" type="text" placeholder="email" value={form.email} onChange={setForm("email")} />
            <Form.Control className="my-3" type="text" placeholder="password" value={form.password} onChange={setForm("password")} />
            <Button className="my-3" variant="primary" type="submit">Submit</Button>
        </Form>
    )
}