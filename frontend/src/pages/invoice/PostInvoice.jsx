import { Button, Form } from "react-bootstrap"
import { useApi } from "../../hooks/useApi"
import { useForm } from "../../hooks/useForm"

export function PostInvoice(){

    const {request} = useApi()
    const {form, setForm} = useForm({
        description : "",
        price : "",
        idAcc : ""
    })

    const onSubmit = async (e)=>{
        e.preventDefault()
        const data = {
            ...form
        }
        request("/invoice", "post", data)
    }

    return (
        <Form onSubmit={onSubmit}>
            <h3>Create invoice</h3>
            <Form.Control className="my-3" type="text" placeholder="description" value={form.description} onChange={setForm("description")} />
            <Form.Control className="my-3" type="number" placeholder="price" value={form.price} onChange={setForm("price")} />
            <Form.Control className="my-3" type="text" placeholder="client id" value={form.idAcc} onChange={setForm("idAcc")} />
            <Button className="my-3" variant="primary" type="submit">Submit</Button>
        </Form>
    )
}