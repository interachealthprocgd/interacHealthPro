import { Button } from "react-bootstrap"
import { useDispatch, useSelector } from "react-redux"
import { useApi } from "../hooks/useApi"
import { authSlice } from "../redux/slices/authSlice"

export function AuthBoard(){

    const {idUser, accountType, email} = useSelector(state => state.authSlice)
    const {request} = useApi()
    const dispatch = useDispatch()

    const onClick = async ()=>{
        const res = await request("/auth/logout")

        if(res.message)
            dispatch(authSlice.reset())

    }

    return(
        <div>
            <div>
                <b>status : </b>
                {idUser ? "You are logged in" : "You are not logged in"}
            </div>
            <div><b>idUser : </b>{"" + idUser}</div>
            <div><b>email : </b>{"" + email}</div>
            <div><b>account type : </b>{"" + accountType}</div>
            <Button variant="primary"  onClick={onClick}>Logout</Button>
        </div>
    )
}