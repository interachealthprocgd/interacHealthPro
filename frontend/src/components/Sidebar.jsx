//import { Stack } from "react-bootstrap";
import { Link } from "react-router-dom";

export function Sidebar() {
  return (
    <div>
      <h4>Routes</h4>
      <div className="my-2">
        <div>/auth/login</div>
        <Link to="/login">POST</Link>
      </div>
     

      <div className="my-2">
        <div>/auth/register/client</div>
        <Link to="/register/client">POST</Link>
      </div>

      <div className="my-2">
        <div>/auth/register/Business</div>
        <Link to="/register/business">POST</Link>
      </div>

      <div className="my-2 d-flex flex-column">
        <div>/business/employee</div>
        <Link to="/employee/get">GET</Link>
        <Link to="/employee/post">POST</Link>
        <Link to="/employee/put">PUT</Link>
        <Link to="/employee/delete">DELETE</Link>
      </div>

      <div className="my-2 d-flex flex-column">
        <div>/business/groupEmployee</div>
        <Link to="/groupEmployee/get">GET</Link>
        <Link to="/groupEmployee/post">POST</Link>
        <Link to="/groupEmployee/put">PUT</Link>
      </div>

      <div className="my-2 d-flex flex-column">
        <div>/paymentInfo</div>
        <Link to="/paymentInfo/get">GET</Link>
        <Link to="/paymentInfo/post">POST</Link>
        <Link to="/paymentInfo/put">PUT</Link>
        <Link to="/paymentInfo/delete">DELETE</Link>
        <div>/invoice</div>
        <Link to="/invoice/post">POST</Link>
      </div>

      <div className="my-2 d-flex flex-column">
        <div>/client/invoice</div>
        <Link to="/client/invoice/get">GET</Link>
      </div>

      <div className="my-2 d-flex flex-column">
        <Link to="/payInvoice">Pay invoice</Link>
      </div>
    </div>
  );
}
