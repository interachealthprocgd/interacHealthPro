import { useSelector } from "react-redux"

export function InfoBoard(){

    const {request, response} = useSelector(state => state.infoSlice)

    return (
        <>
            <div className="my-4 col-6">
                <h3>Request</h3>
                <div><b>url : </b>{request.url}</div>
                <div>
                    <b>Body : </b>
                    <div>{request.body}</div>
                </div>
            </div>

            <div className="my-4 col-6">
                <h3>Response</h3>
                <div><b>Status code : </b>{response.statusCode}</div>
                <div>
                    <b>Body : </b>
                    <div>{response.body}</div>
                </div>
            </div>
        </>
    )
}