import { Outlet } from "react-router-dom";
import { AuthBoard } from "./components/AuthBoard";
import { InfoBoard } from "./components/InfoBoard";
import { Sidebar } from "./components/Sidebar";

export function App(){
  return(
    <div className="container row m-auto">
      <div className="col-12">
        <AuthBoard/>
      </div>
      <div className="col-3">
        <Sidebar />
      </div>
      <div className="col-9">
        <Outlet />
      </div>
      <div className="col-12 row">
        <InfoBoard />
      </div>
      
    </div>
  )
}
