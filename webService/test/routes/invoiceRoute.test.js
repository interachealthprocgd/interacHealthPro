import { expect } from "chai";
import chai from "chai"
import { app } from "../../src/app.js";

const route = "/invoice"
describe(route, ()=>{
    let req = null

    beforeEach(async ()=> req = chai.request(app))
    
    describe("GET", ()=>{
        it("200 - Get all invoices", async() => {
            const res = await req.get(route)

            expect(res.status).is.equal(200)
            expect(res.body).is.instanceOf(Array)
        })
    })

    describe("POST", ()=>{
        it("400 - Missing attribute : description", async ()=>{
            const body = {
                price : 100.00,
                idAcc : "cli-001"
            }
            const res = await req.post(route).send(body)
            expect(res.status).is.equal(400)
        })

        it("400 - client does not exist", async ()=>{
            const body = {
                description : "blood test",
                price : 100.00,
                idAcc : "cli-000"
            }
            const res = await req.post(route).send(body)
            expect(res.status).is.equal(400)
        })

        it("200 - Create Invoice", async ()=>{
            const body = {
                description : "blood test",
                price : 100.00,
                idAcc : "cli-001"
            }
            const res = await req.post(route).send(body)
            expect(res.status).is.equal(200)
        })
    })
})