import { expect } from "chai";
import { before } from "mocha";
import { app } from "../../src/app.js";
import chai from "chai"
import { businessJwt, clientJwt } from "../index.test.js";

const route = "/business/employee";
const cookie = "access_token=" + businessJwt
describe(route, () => {
  
  describe("POST", () => {
    it("400 - Specified idAcc is already an employee", async () => {
      const body = {
        idAcc: "168f9a04-a67b-49e9-8f87-fb0c1d299a80",
        idGroup: null,
      };

      const res = await chai.request(app)
        .post(route)
        .set("cookie", cookie)
        .send(body);

      expect(res.status).to.equal(400);
    });

    it("400 - Specified idAcc doest not exist", async () => {
      const body = {
        idAcc: "cli-000",
        idGroup: null,
      };

      const res = await chai.request(app)
        .post(route)
        .set("Cookie", cookie)
        .send(body)

      expect(res.status).to.equal(400);
    });

    it("400 - Specified idGroup does not exist", async () => {
      const body = {
        idAcc: "cli-001",
        idGroup: "empG-0000",
      };

      const res = await chai.request(app)
        .post(route)
        .set("Cookie", cookie)
        .send(body)

      expect(res.status).to.equal(400);
    });

    it("200 - Employee added with valid idAcc and null idGroup", async () => {
      const body = {
        idAcc: "cli-001",
        idGroup: null,
      };

      const res = await chai.request(app)
        .post(route)
        .set("Cookie", cookie)
        .send(body)

      expect(res.status).to.equal(200);
    });

    it("200 - Employee added with valid idAcc and idGroup", async () => {
      const body = {
        idAcc: "cli-001",
        idGroup: "empG-001",
      };

      const res = await chai.request(app)
        .post(route)
        .set("Cookie", cookie)
        .send(body)

      expect(res.status).to.equal(200);
    });

    it("400 - New account already exist");

    it("200 - New account created and added as employee");
  });

  describe("PUT", () => {
    it("400 - Specified idGroup does not exist", async () => {
      const body = {
        idAcc: "168f9a04-a67b-49e9-8f87-fb0c1d299a80",
        idGroup: "empG-000",
      };

      const res = await chai.request(app)
        .put(route)
        .set("Cookie", cookie)
        .send(body)

      expect(res.status).is.equal(400);
    });

    it("400 - Specified employee id does not exist", async () => {
      const body = {
        idAcc: "cli-001",
        idGroup: "empG-002",
      };
      
      const res = await chai.request(app)
        .put(route)
        .set("Cookie", cookie)
        .send(body)

      expect(res.status).is.equal(400);
    });

    it("200 - Employee updated with valid idGroup", async () => {
      const body = {
        idAcc: "168f9a04-a67b-49e9-8f87-fb0c1d299a80",
        idGroup: "empG-002",
      };
      
      const res = await chai.request(app)
        .put(route)
        .set("Cookie", cookie)
        .send(body)

      expect(res.status).is.equal(200);
    });

    it("200 - Employee updated with null idGroup", async () => {
      const body = {
        idAcc: "168f9a04-a67b-49e9-8f87-fb0c1d299a80",
        idGroup: null,
      };
      
      const res = await chai.request(app)
        .put(route)
        .set("Cookie", cookie)
        .send(body)

      expect(res.status).is.equal(200);
    });
  });

  describe("DELETE", () => {
    it("400 - Employee id does not exist", async () => {
      const body = {
        idAcc: "cli-002",
      };

      const res = await chai.request(app)
      .delete(route)
      .set("Cookie", cookie)
      .send(body)

      expect(res.status).is.equal(400);
    });

    it("200 - Employee deleted", async () => {
      const body = {
        idAcc: "168f9a04-a67b-49e9-8f87-fb0c1d299a80",
      };

      const res = await chai.request(app)
      .delete(route)
      .set("Cookie", cookie)
      .send(body)

      expect(res.status).is.equal(200);
    });
  });

  describe("GET", () => {
    it("200 - Get all employees", async () => {
      const res = await chai.request(app)
      .get(route)
      .set("Cookie", cookie)

      expect(res.status).is.equal(200);
    });
  });
});
