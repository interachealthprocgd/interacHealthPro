import { AuthType } from "../../src/enums/AuthType.js"

export const accounts = [
    {
        idAcc : "168f9a04-a67b-49e9-8f87-fb0c1d299a80",
        lastName : "Kathiresu",
        name : "Caven",
        email : "caven@gmail.com",
        lastUpdateDate : "2023-01-25" , 
        authType : AuthType.EMAIL,
        accActive : true
    },
    {
        idAcc : "cli-001",
        lastName : "Hernandez-Smith",
        name : "Ana",
        email : "anah@gmail.com",
        lastUpdateDate : "2023-01-25" , 
        authType : AuthType.EMAIL,
        accActive : true
    }
]

    

