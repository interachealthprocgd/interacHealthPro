import { TypeCard } from "../../src/enums/TypeCard.js";

export const paymentInfo = [
  {
    idPaymentInfo: "paym-001",
    cardHolder: "Business1",
    numCard: 1234567891234567,
    typeCard: TypeCard.VISA,
    expDate: 1226,
    secCode: 123,
    accActive: true,
    lastUpdateDate: "2023-02-10",
    idAcc: null,
    idAccEnt: "f02b19e0-a1e6-48ed-97c6-60fabebda006",
  }
];
