import { assert, expect } from "chai";
import * as accountRepo from "../../src/repository/accountRepo.js";

describe("Account repository", () => {
  describe("function findOneById()", () => {
    it("valid id should return one", async () => {
      const id = "cli-001";
      const client = await accountRepo.findOneById(id);

      expect(client).is.not.null;
    });

    it("invalid id should return null", async () => {
      const id = "cli-0000";
      const client = await accountRepo.findOneById(id);

      expect(client).is.null;
    });
  });
});
