import paypal from "@paypal/checkout-server-sdk/lib/core/lib.js";
import * as dotenv from "dotenv";


dotenv.config()
const clientId = process.env.PAYPAL_CLIENT_ID
const clientSecret = process.env.PAYPAL_CLIENT_SECRET
const Environment = process.env.NODE_ENV === "production" ? paypal.LiveEnvironment : paypal.SandboxEnvironment
export const paypalClient = new paypal.PayPalHttpClient(new Environment(clientId, clientSecret))