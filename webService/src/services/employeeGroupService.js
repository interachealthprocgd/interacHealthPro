import { BadRequestError } from "../errors/BadRequestError.js";
import { EmployeeGroup } from "../models/EmployeeGroup.js";
import * as EmployeeGroupRepo from "../repository/employeeGroupRepo.js"
import { v4 as uuid } from "uuid";

export async function getAllEmployeeGroups(idAccEnt) {
  return EmployeeGroupRepo.findAllByBusinessId(idAccEnt)
}

export async function createEmployeeGroup(idAccEnt, groupName) {
  const [group, created] = await EmployeeGroup.findOrCreate({
    where: {
      groupName,
      idAccEnt
    }, 

    defaults : {
      idGroup : uuid(),
      groupName,
      accActive : true,
      lastUpdateDate : new Date(),
      idAccEnt
    }
  });
  if (created) {
    return "Group created.";
  } else {
    throw new BadRequestError(
      "Group cannot be created since it already exists."
    );
  }
}

export async function updateEmployeeGroup(
  idGroup,
  groupName,
  idAccEnt,
  accActive
) {
  const [updatedRows] = await EmployeeGroup.update(
    {
      groupName: groupName,
      accActive: accActive,
      lastUpdateDate: new Date(),
    },
    {
      where: { idGroup: idGroup },
    }
  );

  if (updatedRows) {
    return `Updated rows: ${updatedRows}.`;
  } else {
    throw new BadRequestError("Group was not found");
  }
}
