import { Op } from "sequelize";
import { BadRequestError } from "../errors/BadRequestError.js";
import * as paymentInfoRepo from "../repository/paymentInfoRepo.js";
import { PaymentInfo } from "../models/PaymentInfo.js";
import {AccountType} from "../enums/AccountType.js"
import { v4 as uuid } from "uuid";

const accTypeClient = {
  accField: "idAcc",
  accToBeNull: "idAccEnt",
};

const accTypeBusiness = {
  accField: "idAccEnt",
  accToBeNull: "idAcc",
};



export async function getAllPaymentInfo(idUser, role) {
  return paymentInfoRepo.findAll(
    role === AccountType.CLIENT ? idUser : null,
    role === AccountType.BUSINESS ? idUser : null,
  )
}

function msgsPaymentCreated(created) {
  if (created) {
    return "Your payment information has been added to our records.";
  } else if (created === false) {
    throw new BadRequestError(
      "Your payment information cannot be added cannot since it already exists."
    );
  }
}

export async function createPaymentInfo(
  idUser,
  role,
  cardHolder,
  numCard,
  typeCard,
  expDate,
  secCode
) {
  const idAcc = role === AccountType.CLIENT ? idUser : null
  const idAccEnt = role === AccountType.BUSINESS ? idUser : null
  const paymentInfo = PaymentInfo.build({
    idPaymentInfo : uuid(),
    cardHolder,
    numCard,
    typeCard,
    expDate,
    secCode,
    accActive : true,
    lastUpdateDate : new Date(),
    idAcc,
    idAccEnt
  })

  await paymentInfo.validate()
  const existingPayment = await paymentInfoRepo.findOneByCardNumber(numCard, idAcc, idAccEnt)

  if(existingPayment)
    throw new BadRequestError("Your payment information cannot be added cannot since it already exists.");

  await paymentInfo.save()
}

export async function updatePaymentInfo(
  idPaymentInfo,
  idUser,
  role,
  cardHolder,
  numCard,
  typeCard,
  expDate,
  secCode
) {
  const idAcc = role === AccountType.CLIENT ? idUser : null
  const idAccEnt = role === AccountType.BUSINESS ? idUser : null
  const existingPayment = await paymentInfoRepo.findOneById(idPaymentInfo, idAcc, idAccEnt, true)

  if(!existingPayment)
    throw new BadRequestError("The payment information was not found")

  return existingPayment.update({
    cardHolder,
    numCard,
    typeCard,
    expDate,
    secCode,
    lastUpdateDate : new Date()
  })
}

export async function deactivatePaymentInfo(idPaymentInfo, idUser, role) {
  const idAcc = role === AccountType.CLIENT ? idUser : null
  const idAccEnt = role === AccountType.BUSINESS ? idUser : null
  const existingPayment = await paymentInfoRepo.findOneById(idPaymentInfo, idAcc, idAccEnt, true)

  if(!existingPayment)
    throw new BadRequestError("The payment information was not found")

  return existingPayment.update({
    accActive : false,
    lastUpdateDate : new Date()
  })
}
