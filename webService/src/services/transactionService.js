import { sequelize } from "../config/sequelizeConfig.js";
import { BadRequestError } from "../errors/BadRequestError.js";
import { PaypalError } from "../errors/PaypalError.js";
import { Transaction } from "../models/Transaction.js";
import * as InvoiceRepo from "../repository/invoiceRepo.js"
import * as PaypalService from "./paypalService.js"
import { AccountType } from "../enums/AccountType.js";

export async function createOrder(idInvoice){
    const invoice = await InvoiceRepo.findOneById(idInvoice, false)

    if(!invoice)
        throw new BadRequestError("Cannot find the invoice.")

    const paypalResponse = await PaypalService.createOrder(invoice.dataValues)

    if(!paypalResponse.result)
        throw new PaypalError()

    return paypalResponse.result.id
}

export async function capturePayment(orderId, userRole, userId){
   const paypalResponse = await PaypalService.capturePayment(orderId)

   if(!paypalResponse.result)
    throw new PaypalError()

    const {id, purchase_units} = paypalResponse.result
    const referenceId = purchase_units[0].reference_id

    const invoice = await InvoiceRepo.findOneById(referenceId)
    const transaction = Transaction.build({
        idTransaction : id,
        isApproved : true,
        dateTransaction : new Date(),
        idAcc : userRole === AccountType.CLIENT ? userId : null,
        idAccEnt : userRole === AccountType.BUSINESS ? userId : null,
        idInvoice : referenceId
    })

    invoice.set({
        isPayed : true,
        lastUpdate : new Date()
    })

    await sequelize.transaction(async (t) =>{
        await invoice.save({transaction : t})
        await transaction.save({transaction : t})
    })

    return transaction
}