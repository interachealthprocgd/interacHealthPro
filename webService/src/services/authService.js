import { AuthType } from "../enums/AuthType.js";
import { BadRequestError } from "../errors/BadRequestError.js";
import { Account } from "../models/Account.js";
import { Business } from "../models/Business.js";
import * as AccountRepo from "../repository/accountRepo.js"
import * as BusinessRepo from "../repository/businessRepo.js"
import * as KeycloakService from "../services/keycloakService.js"
import {AccountType} from "../enums/AccountType.js"

export async function registerClient({
  idAcc,
  name,
  lastName,
  email,
  password,
}) {
  const clientAccount = Account.build({
    idAcc,
    name,
    lastName,
    email,
    password,
    lastConnecDate: new Date(),
    lastUpdateDate: new Date(),
    accTypeAuth: AuthType.EMAIL,
    accType: AccType.INDIVIDUAL,
    accActive: true,
  });

  await clientAccount.validate();
  const account = await Account.findByPk(idAcc);

  if (account)
    throw new BadRequestError(
      "Unable to register client. This account already exist."
    );

  return await clientAccount.save();
}

export async function registerBusiness({email, nameEnt, nameContact, emailContact, telNumber, password}) {
  const business = Business.build({
    nameEnt,
    email,
    nameContact,
    emailContact,
    telNumber,
    accActive : true,
    lastUpdateDate : new Date()
  });

  await business.validate({skip : ["idAccEnt"]});

  const idUser = await KeycloakService.registerUser(email, password, AccountType.BUSINESS, nameEnt)
  console.log(idUser)
  business.idAccEnt = idUser
  await business.save();
}

export async function loginClient(email, password){
  const account = await AccountRepo.findOneByEmail(email, true)

  if(!account)
    throw new BadRequestError("Invalid email")

  if(password !== account.password)
    throw new BadRequestError("Invalid password")

  return account.idAcc
}

export async function loginBusiness(idBusiness, password){
  const business = await BusinessRepo.findOneById(idBusiness, true)

  if(!business)
    throw new BadRequestError("Invalid business id")

  if(password !== business.password)
    throw new BadRequestError("Invalid password")

}

