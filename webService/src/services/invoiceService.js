import { BadRequestError } from "../errors/BadRequestError.js";
import { Invoice } from "../models/Invoice.js";
import {v4 as uuid} from "uuid"
import * as AccountRepo from "../repository/accountRepo.js"
import * as InvoiceRepo from "../repository/invoiceRepo.js"

export function getAllInvoicesByClientId(idAcc, isPayed){
    return InvoiceRepo.findAllByClientId(idAcc, isPayed)
}

export function getAllInvoices(){
    return Invoice.findAll()
}

export async function create({description, price, idAcc}){

    const invoice = Invoice.build({
        idInvoice : uuid(),
        description,
        price,
        idAcc,
        isPayed : false,
        lastUpdate : new Date()
    })

    await invoice.validate()
    const existingAccount = await AccountRepo.findOneById(idAcc, true)

    if(!existingAccount)
        throw new BadRequestError("Unable to create invoice. The specified client does not exist")
        
    return invoice.save()
} 