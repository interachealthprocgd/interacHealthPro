import { BadRequestError } from "../errors/BadRequestError.js";
import { Employee } from "../models/Employee.js";
import * as authService from "./authService.js"
import * as employeeRepo from "../repository/employeeRepo.js"
import * as accountRepo from "../repository/accountRepo.js"
import * as groupEmployeeRepo from "../repository/employeeGroupRepo.js"

export async function getEmployees(idAccEnt){
    const employees = await employeeRepo.findAllByBusinessId(idAccEnt)

    return employees.map(e => {
        const {idAcc, idGroup, accActive, lastUpdateDate} = e.dataValues
        const {name, lastName, email} = e.Account.dataValues
        let result = {idAcc, idGroup, accActive, lastUpdateDate, name, lastName, email}

        if(idGroup !== null){
            const {groupName} = e.EmployeeGroup.dataValues
            result = {...result, groupName}
        }

        return result
    })
}

export async function createEmployee(idAccEnt, idAcc, idGroup){

    const employee = Employee.build({
        idAccEnt,
        idAcc,
        idGroup,
        accActive : true,
        lastUpdateDate : new Date()
    })

    await employee.validate()

    const existingEmployee = await employeeRepo.findOneById(idAcc, idAccEnt)
    const existingAccount = await accountRepo.findOneById(idAcc, true)

    if(existingEmployee)
        throw new BadRequestError("This account is already your employee.")

    if(!existingAccount)
        throw new BadRequestError("Unable to add employee. The specified account does not exist.")

    if(idGroup){
        const existingGroup = await groupEmployeeRepo.findOneById(idGroup, idAccEnt, true)

        if(!existingGroup)
        throw new BadRequestError("Unable to add employee. The specified employee group does not exist.")
    }
   
    await employee.save()
}

export async function registerClientAndAddEmployee(idAccEnt, idGroup, newAccount){
    const client = await authService.registerClient(newAccount)
    await createEmployee(idAccEnt, client.dataValues.idAcc, idGroup)
}

export async function updateEmployee(idAccEnt, idAcc, idGroup){
    if(idGroup !== null){
        const existingGroup = await groupEmployeeRepo.findOneById(idGroup, idAccEnt, true)
        if(!existingGroup)
            throw new BadRequestError("Unable to update employee. The specified employee group does not exist.")
    }

    const [rows] = await employeeRepo.updateOneById(idAcc, idAccEnt, {idGroup}, true)

    if(rows === 0)
        throw new BadRequestError("Employee not found.")
}

export async function deactivateEmployee(idAccEnt, idAcc){
    const [row] = await employeeRepo.updateOneById(idAcc, idAccEnt, {accActive : false}, true)

    if(row === 0)
        throw new BadRequestError("Employee not found.")
}

