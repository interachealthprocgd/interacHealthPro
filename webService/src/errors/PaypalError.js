

export class PaypalError extends Error{
    constructor(message = "An error occured on paypal transaction"){
        super(message)
        this.name = this.constructor.name
    }
}