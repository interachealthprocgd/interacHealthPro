
export class NotLoggedInError extends Error{
    constructor(message = "You are not logged in"){
        super(message)
        this.name = this.constructor.name
    }
}