import "./models/tableAssociation.js";
import express from "express";
import cookieParser from "cookie-parser";
import { corsConfig } from "./config/corsConfig.js";
import { authRoute } from "./routes/authRoute.js";
import { errorHandler } from "./middleware/errorHandler.js";
import { employeeGroupRoute } from "./routes/employeeGroupRoute.js";
import { employeeRoute } from "./routes/employeeRoute.js";
import { paymentInfoRoute } from "./routes/paymentInfoRoute.js";
import { invoiceRoute } from "./routes/invoiceRoute.js";
import { clientInvoiceRoute } from "./routes/clientInvoiceRoute.js";
import { transactionRoute } from "./routes/transactionRoute.js";
import { jwtMiddleware } from "./middleware/jwtMiddleware.js";

export const app = express();

// Config
app.use(express.json());
app.use(corsConfig);
app.use(cookieParser())
app.use(jwtMiddleware)
// app.use(sessionConfig)

// app.use((req, res, next)=>{
//     console.log(req.session)
//     next()
// })





// Routes
app.get("/", (req, res) => res.send("Service web Interact Health Pro"));
app.use("/auth", authRoute);

// Business routes
app.use("/business/groupEmployee", employeeGroupRoute);
app.use("/business/employee", employeeRoute);
app.use("/paymentInfo", paymentInfoRoute);

// Invoice route
app.use("/invoice", invoiceRoute)

app.use("/client/invoice", clientInvoiceRoute)
// transaction
app.use("/transaction", transactionRoute)

// Error handler
app.use(errorHandler);

