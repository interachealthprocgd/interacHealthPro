import { Account } from "../models/Account.js";
import { Employee } from "../models/Employee.js";
import { EmployeeGroup } from "../models/EmployeeGroup.js";

export function findOneById(idAcc, idAccEnt, isActive = undefined) {
  return Employee.findOne({
    where: {
      idAcc,
      idAccEnt,
      accActive: isActive === undefined ? [true, false] : [isActive],
    },
  });
}

export function findAllByBusinessId(idAccEnt, isActive = undefined) {
  return Employee.findAll({
    where: {
      idAccEnt,
      accActive: isActive === undefined ? [true, false] : [isActive],
    },
    include: [Account, EmployeeGroup],
  });
}

export function updateOneById(idAcc, idAccEnt, data, isActive = undefined) {
  return Employee.update(
    {
      ...data,
      lastUpdateDate: new Date(),
    },
    {
      where: {
        idAcc,
        idAccEnt,
        accActive: isActive === undefined ? [true, false] : [isActive],
      },
    }
  );
}
