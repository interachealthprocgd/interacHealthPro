import { Account } from "../models/Account.js";

export function findOneById(idAcc, isActive = undefined){
    return Account.findOne({
        where : {
            idAcc,
            accActive : isActive === undefined ? [true, false] : [isActive]
        }
    })
}

export function findOneByEmail(email, isActive = undefined){
    return Account.findOne({
        where : {
            email,
            accActive : isActive === undefined ? [true, false] : [isActive]
        }
    })
}