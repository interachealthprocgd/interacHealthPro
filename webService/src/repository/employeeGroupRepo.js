import { EmployeeGroup } from "../models/EmployeeGroup.js";

export function findOneById(idGroup, idAccEnt, isActive = undefined) {
  return EmployeeGroup.findOne({
    where: {
      idGroup,
      idAccEnt,
      accActive: isActive === undefined ? [true, false] : [isActive],
    }
  });
}

export function findAllByBusinessId(idAccEnt, isActive = undefined){
  return EmployeeGroup.findAll({
    where : {
      idAccEnt,
      accActive: isActive === undefined ? [true, false] : [isActive]
    }
  })
}
