import { Invoice } from "../models/Invoice.js";

export function findAllByClientId(idAcc, isPayed = undefined){
    return Invoice.findAll({
        where : {
            idAcc,
            isPayed : isPayed === undefined ? [true, false] : [isPayed]
        }
    })
}

export function findOneById(idInvoice, isPayed = undefined){
    return Invoice.findOne({
        where : {
            idInvoice,
            isPayed : isPayed === undefined ? [true, false] : [isPayed]
        }
    })
}