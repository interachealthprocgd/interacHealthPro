import { Router } from "express";
import * as InvoiceService from "../services/invoiceService.js"
import * as AuthMiddleware from "../middleware/authMiddleware.js"
import { AccountType } from "../enums/AccountType.js";

export const clientInvoiceRoute = Router()
clientInvoiceRoute.use(AuthMiddleware.isAuthenticated)
clientInvoiceRoute.use(AuthMiddleware.isRole([AccountType.CLIENT]))
clientInvoiceRoute.get("/", getInvoices)

async function getInvoices(req, res, next){
    try{
        const {idUser} = req.session
        const invoices = await InvoiceService.getAllInvoicesByClientId(idUser)
        res.json(invoices)
    } catch (error){
        next(error)
    }
}