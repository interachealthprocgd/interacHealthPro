import { Router } from "express";
import * as paymentInfoService from "../services/paymentInfoService.js";
import * as AuthMiddleware from "../middleware/authMiddleware.js"

export const paymentInfoRoute = Router();
paymentInfoRoute.use(AuthMiddleware.isAuthenticated)
paymentInfoRoute.get("/getAllPaymentInfo", getAllPaymentInfo);
paymentInfoRoute.post("/createPaymentInfo", createPaymentInfo);
paymentInfoRoute.put("/updatePaymentInfo", updatePaymentInfo);
paymentInfoRoute.delete("/deletePaymentInfo", deletePaymentInfo);

async function getAllPaymentInfo(req, res, next) {
  try {
    const {idUser, role} = req.session

    const paymentInfoList = await paymentInfoService.getAllPaymentInfo(idUser, role);
    res.json(paymentInfoList);
  } catch (error) {
    next(error);
  }
}

async function createPaymentInfo(req, res, next) {
  try {
    const {idUser, role} = req.session

    const paymentInfo = await paymentInfoService.createPaymentInfo(
      idUser,
      role,
      req.body.cardHolder,
      req.body.numCard,
      req.body.typeCard,
      req.body.expDate,
      req.body.secCode
    );
    res.json({message : "paymentInfo created"});
  } catch (error) {
    next(error);
  }
}

async function updatePaymentInfo(req, res, next) {
  try {
    const {idUser, role} = req.session
    const paymentInfoUpdated = await paymentInfoService.updatePaymentInfo(
      req.body.idPaymentInfo,
      idUser,
      role,
      req.body.cardHolder,
      req.body.numCard,
      req.body.typeCard,
      req.body.expDate,
      req.body.secCode,
    );
    res.json({message : "payment info updated"});
  } catch (error) {
    next(error);
  }
}

async function deletePaymentInfo(req, res, next) {
  try {
   const {idUser,  role} = req.session
   const {idPaymentInfo} = req.body

    await paymentInfoService.deactivatePaymentInfo(idPaymentInfo, idUser, role);
    res.json({ message: "Your payment record has been deleted" });
  } catch (error) {
    next(error);
  }
}
