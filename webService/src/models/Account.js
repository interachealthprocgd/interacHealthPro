import { BOOLEAN, DATE, STRING } from "sequelize";
import { sequelize } from "../config/sequelizeConfig.js";
import { AuthType } from "../enums/AuthType.js";

export const Account = sequelize.define(
  "Account",
  {
    idAcc: {
      type: STRING,
      primaryKey: true,
      allowNull: false,
      validate : {
        notEmpty : true
      }
    },

    lastName: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    },

    name: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    },

    email: {
      type: STRING,
      allowNull: false,
      validate: {
        isEmail: true,
      }
    },

    lastUpdateDate: {
      type: DATE,
      allowNull: false,
      validate: {
        isDate: true,
      }
    },

    authType: {
      type: STRING,
      allowNull: false,
      validate: {
        isIn: [
          [AuthType.APPLE, AuthType.EMAIL, AuthType.FACEBOOK, AuthType.GMAIL],
        ]
      }
    },

    accActive: {
      type: BOOLEAN,
      allowNull: false,
      validate: {
        isIn: [[true, false]]
      }
    }
  },

  {
    tableName: "client",
    timestamps: false
  }
);
