import { DataTypes, INTEGER, STRING } from "sequelize";
import { sequelize } from "../config/sequelizeConfig.js";

export const EmployeeGroup = sequelize.define(
  "EmployeeGroup",
  {
    idGroup: {
      type: STRING,
      primaryKey: true,
      allowNull: false,
      validate: {
        notEmpty : true
      },
    },

    groupName: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty : true
      },
    },

    accActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      validate: {
        isIn: [[true, false]],
      },
    },

    lastUpdateDate: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        isDate: true,
      },
    },

    idAccEnt: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty : true
      },
    },
  },

  {
    tableName: "employeeGroup",
    timestamps: false,
  }
);
