import { BOOLEAN, DATE, INTEGER, STRING } from "sequelize";
import { sequelize } from "../config/sequelizeConfig.js";

export const Transaction = sequelize.define(
    "Transaction",
    {
        idTransaction : {
            type : STRING,
            primaryKey : true,
            allowNull : false,
            validate : {
                notEmpty : true
            }
        },

        dateTransaction : {
            type : DATE,
            allowNull : false,
            validate : {
                isDate : true
            }
        },

        isApproved : {
            type : BOOLEAN,
            allowNull : false,
            validate : {
                isIn : [[true, false]]
            }
        },

        idAcc : {
            type : STRING,
            allowNull : true,
            validate : {
                notEmpty : true
            }
        },

        idAccEnt : {
            type : STRING,
            allowNull : true,
            validate : {
                notEmpty : true
            }
        },

        idInvoice : {
            type : STRING,
            allowNull : false,
            validate : {
                notEmpty : true
            }
        }
    }, 
    {
        tableName : "transaction",
        timestamps : false
    }
)