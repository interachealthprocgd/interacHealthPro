
export function errorHandler(err,req, res, next){
    const {name, message} = err

    // console.log(name)
    // console.log(err)

    if(name === "SequelizeValidationError")
        res.status(400).json({message : err.errors.map(e => e.message)})

    else if(name === "BadRequestError")
        res.status(400).json({message})

    else if(name === "PaypalError")
        res.status(405).json({message})
    
    else if(name === "NotLoggedInError")
        res.status(401).json({message})

    else if(name === "NotAuthorizedError")
        res.status(403).json({message})

    else
        res.status(405).json({message : "Unknown error occured"})
}