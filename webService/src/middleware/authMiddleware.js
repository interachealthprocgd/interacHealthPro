import { NotAuthorizedError } from "../errors/NotAuthorizedError.js"
import { NotLoggedInError } from "../errors/NotLoggedInError.js"

export function isRole(roles){
    return (req, res, next)=>{
        if(!roles.includes(req.session?.role))
            throw new NotAuthorizedError()

        next()
    }
}

export function isAuthenticated(req, res, next){
    if(!req.session?.idUser)
        throw new NotLoggedInError()

    next()
}

export function isNotAuthenticated(req, res, next){
    if(req.session?.idUser)
        throw new NotAuthorizedError("You must be logged out to use this ressource.")

    next()
}