DELETE FROM "transaction";
DELETE FROM "invoice";
DELETE FROM "employee";
DELETE FROM "employeeGroup";
DELETE FROM "businessAcc";
DELETE FROM "client";

insert into "client" values 
('168f9a04-a67b-49e9-8f87-fb0c1d299a80', 'Kathiresu', 'Caven', 'caven@gmail.com', '2023-01-25', 'Email', true),
('cli-001', 'Hernandez-Smith', 'Ana', 'anah@gmail.com', '2023-01-25', 'Email', true);

insert into "businessAcc" values
('f02b19e0-a1e6-48ed-97c6-60fabebda006', 'business 1', 'business1@gmail.com', 'kavya i', 'kavya@gmail.com', '5149974434', '2023-01-23', true),
('bus-002', 'business 2', 'business2@gmail.com', 'risha k', 'risha@gmail.com', '5142248734', '2022-12-23', true);

insert into "employeeGroup" values
('empG-001', 'Marketing', true, '2023-01-23', 'f02b19e0-a1e6-48ed-97c6-60fabebda006'),
('empG-002', 'Developer', true, '2023-01-23', 'f02b19e0-a1e6-48ed-97c6-60fabebda006'),
('empG-003', 'Management', true, '2023-01-23', 'bus-002');

insert into "employee" values
('168f9a04-a67b-49e9-8f87-fb0c1d299a80', 'f02b19e0-a1e6-48ed-97c6-60fabebda006', null, true, '2023-01-23');

insert into "paymentInfo" values
('paym-001', 'Business1', 1234567891234567, 'Visa', 1226, 123, true, '2023-02-10', null, 'f02b19e0-a1e6-48ed-97c6-60fabebda006');

insert into "invoice" values
('inv-001', 'Ergothérapie', 100.50, false, '2023-01-23', '168f9a04-a67b-49e9-8f87-fb0c1d299a80'),
('inv-002', 'Examen dentaire', 230.79, false, '2023-01-23', '168f9a04-a67b-49e9-8f87-fb0c1d299a80'),
('inv-003', 'Physiothérapie', 80.00, true, '2023-01-23', 'cli-001');

insert into "transaction" values
('tra-001', '2023-02-12', true, 'cli-001', null, 'inv-003');



